<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Librarygroup extends MY_Controller{
	public function __construct(){
		parent:: __construct();
		$this->table='wstm_page';
	}

	public function getlist(){
		
		$sql = "SELECT t1.id, t1.name, t1.link , t2.count FROM wstm_page as t1

        LEFT JOIN ( SELECT count(id) as count, page_id   FROM wstb_library GROUP BY page_id)  as t2 On t1.id=t2.page_id

        WHERE t1.type = 5 AND t1.status = 1 and t1.id not in (select parent_id from wstm_page) ";
		
		$query=$this->db->query($sql);
	
		$list = $query->result_object();
		
		$message = $this->lang->line('success');
		$this->responsesuccess($message,$list);
	}
	
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends MY_Controller{
	
	function __construct(){
		
		parent:: __construct();
		
	}
	public function getlist(){
		
		$sql="SELECT t1.* , t2.name as name_customer, t2.code as code_customer, t2.email 
		
			FROM ortb_cart AS t1 
			
			LEFT JOIN cstb_customer AS t2 ON t2.id=t1.customer_id 
			
			ORDER BY t1.day_start DESC, t1.id DESC";

			
		$query = $this->db->query($sql);
		
		$list = $query->result_object();
		
		$this->responsesuccess($this->lang->line('success') , $list);
		
	}
	public function getrow(){
		
		$id = $this->params['id'];
		
	
		$sql="SELECT t1.* , t2.name 
		
		FROM ortb_cart AS t1 
		
		LEFT JOIN cstb_customer AS t2 ON t2.id = t1.customer_id 
		
		WHERE t1.id=".$id;
		
		$query = $this->db->query($sql);
		
		$cart = $query->row_object();
		
		$sql="SELECT t1.* , t2.name,t2.code, t2.images 
		
		FROM ortb_cart_detail AS t1 LEFT JOIN pdtb_product AS t2 ON t2.id = t1.product_id WHERE t1.cart_id=".$id;
		
		$query = $this->db->query($sql);
		
		$detail = $query->result_object();
		
		$data = array('cart'=> $cart,'detail'=>$detail);
		
		$this->responsesuccess($this->lang->line('success') , $data);
	}
	
	public function changeStatus(){
		
		$id = $this->params['id'];
		
		$delivery_status = $this->params['delivery_status'];
		
		$res = array('status'=>0,'message'=>'','data'=>array());
		
		$is=false;
		
		$this->db->where('id',$id);
			
		$is=$this->db->update('ortb_cart',array('delivery_status' => $delivery_status, 'checked'=> $delivery_status == 1 ? 0 : 1));
		
		$res['message'] = ($is==true) ? $this->lang->line('success') : $this->lang->line('failure');
		
		$res['status'] = ($is==true) ? 1 : 0;
		
		if($res['status'] ==1 ){
		
			$this->responsesuccess($res['message']);
		}
		else{
			$this->responsefailure($res['message']);
		}
	}
}